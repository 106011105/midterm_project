# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : iLine
* Key functions (add/delete)
    1. 一對一聊天
	2. 群組聊天
	3. 保留歷史聊天紀錄
	4. 添加新好友聊天
    
* Other functions (add/delete)
    1. 所有群組、好友列表
    2. 所有聊天對象列表
    3. 搜尋使用者

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description
<img src="https://i.imgur.com/yUxRfI9.png" title="Sign-In" width="300px">

* 登入頁面
	1. Email&密碼登入
	2. Google登入
	3. 到註冊頁面
	
<img src="https://i.imgur.com/zht2uA4.png" title="Sign-Up Page" width="300px">

* 註冊頁面
	1. 選擇帳號頭像
	2. 輸入稱呼、Email、密碼註冊帳號
	
<img src="https://i.imgur.com/Ln7UjUz.png" width="500px">

* 好友介面
	1. 最上方Navbar顯示"好友"
	2. 下方顯示使用者加入的群組及好友
	3. 點選右方黑色的聊天圖示即可進入聊天室
	
<img src="https://i.imgur.com/FbIoI5V.png" width="500px">

* 加入好友介面
	1. 最上方Navbar顯示"加入好友"
	2. 搜尋框可輸入好友名稱查詢
	3. 底下搜尋結果出現時，按下右方加入即可添加好友
	
<img src="https://i.imgur.com/mj3ld4Y.png" width="500px">

* 聊天列表介面
	1. 最上方Navbar顯示"聊天"
	2. 下方依序列出聊天對象
	3. 點擊列表跳至聊天對話框
	
<img src="https://i.imgur.com/dcucqs7.png" width="500px">

* 聊天介面
	1. 顯示過去所有聊天紀錄
	2. 進入頁面時，自動移動至最新對話框
	3. 顯示對話人物頭像、名稱、訊息及發送時間

# 作品網址：
https://ss-midterm-project-90f4a.firebaseapp.com

# Components Description : 
1. Database read/write : 分別在users、chats節點下設定read&write : auth != null
2. Chrome notification : 當別人傳來訊息，但是不在該對話框時產生

	<img src="https://i.imgur.com/fi9pK7d.png" width="300px">
3. Css animation : 兩個keyframe，slide-down-in和slide-down-out在切換不同介面及退出聊天對話時播放

# Other Functions Description(1~10%) : 
1. 搜尋 : 可以依據關鍵字搜尋好友
2. 新訊息處理 : 隨時保持有新訊息來時，跳至聊天對話最底部 
3. 好友介面 : 顯示好友數量及群組數量

## Security Report (Optional)
1. 對特殊字元進行處理，防止寫入 `<tag></tag>`


	msg = msg.replace(/&/g, "&amp;")
					  .replace(/</g, "&lt;")
					  .replace(/>/g, "&gt;")
					  .replace(/"/g, "&quot;")
					  .replace(/'/g, "&#039;");

2. 使用者只能看到database中自己uid下的Chat List，不會發生看到別人的對話紀錄，或是被其他人看到對話紀錄的情況。










