window.onload = () => {
  let left = document.querySelector('.left');
  let right = document.querySelector('.right');
  let index = 1;

  function clickLeft() {
    index = index == 1 ? 6 : index - 1;
    document.querySelector('.user-img').innerHTML = `<img src="./img/user${index}.png">`
  }

  function clickRight() {
    index = index == 6 ? 1 : index + 1;
    document.querySelector('.user-img').innerHTML = `<img src="./img/user${index}.png">`
  }

  left.addEventListener('click', clickLeft);
  right.addEventListener('click', clickRight);

  let btnSignUp = document.getElementById('btnSignUp');

  btnSignUp.addEventListener('click', () => {
    let name = document.getElementById('inputName').value;
    let email = document.getElementById('inputEmail').value;
    let password = document.getElementById('inputPassword').value;
    firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
      let loginUser = firebase.auth().currentUser;
      firebase.database().ref('users/' + loginUser.uid).set({
        email: loginUser.email,
        name: name,
        img: `user${index}.png`,
        friends: {
          group: [{
            name: 'PlayGround',
            img: 'group3.png'
          }],
          friend: []
        }
      });
      alert('Sign Up Success!');
      setTimeout(() => {
        document.location.href = './index.html'
      }, 1000);
    }).catch(error => {
      alert(error);
      document.getElementById('inputEmail').value = '';
      document.getElementById('inputPassword').value = '';
    });
  });
}