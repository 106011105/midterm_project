const $ = (element) => {return document.querySelector(element)}

let UserUid, UserName, UserImg;
let DB_users, DB_chats, DB_users_uid;
let CurrentRoom;
let SlideIndex = 0;

function changeClass(add, remove, className) {
  remove.forEach(el => el.classList.remove(className));
  add.forEach(el => el.classList.add(className));
}

function initNavbar() {
  let navItems = document.querySelectorAll('.nav-item');
  let logout = $('.logout');
  let toolType = $('.tool-type');
  let backImg = $('.back-img');
  let friends = $('.friends');
  let chat = $('.chat');
  let addFriend = $('.add-friends');
  let talk = $('.talk');
  let talkInput = $('.talk-input');

  //  Frined Item Click
  navItems[0].addEventListener('click', () => {
    if (SlideIndex !== 0) {
      toolType.innerHTML = '好友';
      let add, remove;

      add = [navItems[0]];
      remove = [navItems[1], navItems[2]];
      changeClass(add, remove, 'active');

      add = [backImg, chat, addFriend, talk, talkInput];
      remove = [friends];
      changeClass(add, remove, 'hidden');
      let animation = 'slide-down-in 0.5s'
      friends.style.animation = animation;
      setTimeout(() => {
        friends.style.animation = '';
      }, 500);
      SlideIndex = 0;
    }

  });

  // Chat Item Click
  navItems[1].addEventListener('click', () => {
    if (SlideIndex !== 1) {
      toolType.innerHTML = '聊天';
      let add, remove;
      add = [navItems[1]];
      remove = [navItems[0], navItems[2]];
      changeClass(add, remove, 'active');

      add = [backImg, friends, addFriend, talk, talkInput];
      remove = [chat];
      changeClass(add, remove, 'hidden');

      let animation = 'slide-down-in 0.5s'
      chat.style.animation = animation;
      setTimeout(() => {
        chat.style.animation = '';
      }, 500);
      SlideIndex = 1;
    }
  });

  // Add Friend Item Click
  navItems[2].addEventListener('click', () => {
    if (SlideIndex !== 2) {
      toolType.innerHTML = '加入好友';
      let add, remove;

      add = [navItems[2]];
      remove = [navItems[0], navItems[1]];
      changeClass(add, remove, 'active');

      add = [backImg, friends, chat, talk, talkInput];
      remove = [addFriend];
      changeClass(add, remove, 'hidden');
      let animation = 'slide-down-in 0.5s'
      addFriend.style.animation = animation;
      setTimeout(() => {
        addFriend.style.animation = '';
      }, 500);
      SlideIndex = 2;
    }

  });

  logout.addEventListener('click', () => {
    if (firebase.auth().currentUser) {
      firebase.auth().signOut();
    }
    document.location.href = './login.html';
  });

  // Back Item Click
  backImg.addEventListener('click', () => {
    talk.style.animation = 'slide-down-out 0.5s';
    setTimeout(() => {
      add = [talk, talkInput, backImg];
      remove = [chat, navItems[0], navItems[1], navItems[2]];
      changeClass(add, remove, 'hidden');

      $('.tool').style.height = '60px';
      talk.style.animation = '';

    }, 500);

    DB_chats.child(`${CurrentRoom}/messages`).off();
    SlideIndex = 1;
    CurrentRoom = undefined;
  })
}




function createRoom(friendName, friendImg) {
  DB_users.once('value', snapshot => {
    snapshot.forEach(el => {
      let {name} = el.val();
      if (name === friendName) {
        DB_users.child(el.key + '/chatList').push().set({
          img: UserImg,
          name: UserName
        })
      } else if (name === UserName) {
        DB_users.child(el.key + '/chatList').push().set({
          img: friendImg,
          name: friendName
        })
      }
    });
  });
}

function startTalk(friendName, friendImg) {
  $('.tool').style.height = '0px';

  let navItems = document.querySelectorAll('.nav-item');
  let toolType = $('.tool-type');
  let backImg = $('.back-img');
  let friends = $('.friends');
  let chat = $('.chat');
  let addFriend = $('.add-friends');
  let talk = $('.talk');
  let talkInput = $('.talk-input');
  let add, remove;

  add = [navItems[1]];
  remove = [navItems[0], navItems[2]];
  changeClass(add, remove, 'active');

  toolType.innerHTML = '聊天';

  add = [friends, chat, addFriend, navItems[0], navItems[1], navItems[2]];
  remove = [talk, talkInput, backImg];
  changeClass(add, remove, 'hidden');

  let roomName;
  if (friendName === 'PlayGround') {
    roomName = friendName;
  } else {
    roomName = [UserName, friendName].sort().join('');
  }
  
  CurrentRoom = roomName;

  DB_users_uid.child('chatList').once('value').then(snapshot => {
    newRoom = true;
    snapshot.forEach(el => {
      let name = el.val().name;
      if (name === friendName) {
        newRoom = false;
      }      
    });
    if (newRoom)
      createRoom(friendName, friendImg);
  });


  let talkPosts = '';
  talk.innerHTML = '';
  DB_chats.child(`${roomName}/messages`).on('child_added', function updateMsg(snapshot) {
    let {name, msg, time, img} = snapshot.val();
    if (!name || !msg || !time) return;
    let reverse = name === UserName ? 'reverse' : '';
    talkPosts += `
      <div class="talk-box ${reverse}">
        <div class="user-img">
          <img src="./img/${img}" alt="">
        </div>
        <div class="talk-info ${reverse}">
          <div class="user-name">${name}</div>
          <div class="message ${reverse}">
            <div class="user-message ${reverse}">${msg}</div>
            <div class="send-time">${time}</div>
          </div>
        </div>
      </div>      
    `;
    talk.innerHTML = talkPosts;
    talk.scrollTop = talk.scrollHeight;
  });
}

function initFriends() {

  // Update Friend List
  DB_users_uid.child(`friends/friend`).on('value', snapshot => {
    let items = '', info = '', num = 0;
    snapshot.forEach(el => {
      num++;
      let {name, img} = el.val();
      items += `
        <div class="item">
          <div class="item-img">
            <img src="./img/${img}" alt="">
          </div>
          <div class="item-msg">${name}</div>
          <div class="item-arrow" onclick="startTalk('${name}', '${img}')">
            <i class="fas fa-comment-dots" title="聊天"></i>
          </div>
        </div>
      `;
    })
    info = `
      <div class="info">
        <span>好友</span><span class="friend-num">${num}</span>
      </div>
    `;
    $('.friend').innerHTML = info + items;
  });

  // Update Group List
  DB_users_uid.child(`friends/group`).on('value', snapshot => {
    let items = '', info = '', num = 0;
    snapshot.forEach(el => {
      num++;
      let {name, img} = el.val();
      items += `
        <div class="item">
          <div class="item-img">
            <img src="./img/${img}" alt="">
          </div>
          <div class="item-msg">${name}</div>
          <div class="item-arrow" onclick="startTalk('${name}', '${img}')">
            <i class="fas fa-comment-dots" title="聊天"></i>
          </div>
        </div>
      `
    })
    info = `
      <div class="info">
        <span>群組</span><span class="group-num">${num}</span>
      </div>
    `
    $('.group').innerHTML = info + items;    
  })
}



function initChats() {
  // Update Chat List
  DB_users_uid.child(`/chatList`).on('value', snapshot => {
    let postList = [];
    snapshot.forEach(el => {
      let {name, img} = el.val();
      let postData = `
        <div class="chat-box" onclick="startTalk('${name}', '${img}')">
          <div class="user-photo">
            <img src="./img/${img}" alt="">
          </div>
          <div class="user-info">${name}</div>
        </div>
      `;
      postList.unshift(postData);
    });
    $('.chat').innerHTML = postList.join('');
  })
}



function handleAddFriend(friendName, friendImg) {
  DB_users_uid.child(`friends/friend`).once('value').then(snapshot => {
    let newFriend = true;
    snapshot.forEach(el => {
      let {name, img} = el.val();
      if (name === friendName && img === friendImg) {
        newFriend = false;
        return;
      }
    });
    if (newFriend) {
      DB_users_uid.child(`friends/friend`).push().set({
        name: friendName,
        img: friendImg
      });
      alert('添加好友成功');
    } else {
      alert('重複添加好友無效');
    }
  })
}

function initAddFriend() {

  // Handle Search Friend
  $('#search-friend-input').addEventListener('change', e => {
    let inputValue = e.target.value;
    let items = '';
    let info = `
      <div class="info">
        <span>搜尋結果</span>
      </div>
    `;
    DB_users.once('value').then(snapshot => {
      snapshot.forEach(el => {
        let {name, img} = el.val();
        if (el.key === UserUid) return;
        if (name.toLowerCase().indexOf(inputValue.toLowerCase()) !== -1 && inputValue !== '') {
          items += `
            <div class="item">
              <div class="item-img">
                <img src="./img/${img}" alt="">
              </div>
              <div class="item-msg">${name}</div>
              <button class="add-btn" onclick="handleAddFriend('${name}', '${img}')">加入</button>
            </div>          
          `;
        }
      });
      $('.search-result').innerHTML = info + items;
    });
    DB_chats.child('group');
  });
}




function getTime() {
  let date = new Date();
  let hour = date.getHours();
  let min = date.getMinutes();
  hour = hour < 10 ? '0' + hour : hour;
  min = min < 10 ? '0' + min : min;
  return hour + ':' + min;
}

function initTalk() {
  let inputMsg = $('#inputMsg');
  inputMsg.addEventListener('keydown', e => {
    if (e.keyCode == 13) { // Enter
      let msg = inputMsg.value;
      msg = msg.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
      if (msg != '') {
        let time = getTime();
        DB_chats.child(CurrentRoom + '/messages').push().set({
          name: UserName,
          img: UserImg,
          msg,
          time
        });
        inputMsg.value = '';
      }
    }
  })
}




function initTalkListener() {
  DB_chats.on('child_added', snapshot => {
    if (snapshot.key.indexOf(UserName) !== -1 || snapshot.key === 'PlayGround') {
      DB_chats.child(snapshot.key + '/messages').on('child_added', childSnapshot => {
        let {img, msg, name, time} = childSnapshot.val();
        if (time === getTime() && name !== UserName && CurrentRoom !== snapshot.key) {
          let notifyConfig = {
            body: msg,
            icon: `./img/${img}`
          }
          Notification.requestPermission(function (permission) {
            if (permission === 'granted') { // 使用者同意授權
              new Notification(`${name}`, notifyConfig); // 建立通知
            }
          });
        }
      });  
    }
  });
}

window.onload = () => {

  if (!('Notification' in window)) {
    console.log('This browser does not support notification');
  } else {
    console.log("Notification Support on this browser!");
  }


  firebase.auth().onAuthStateChanged(user => {
    if (!user) {
      document.location.href = './login.html';
    }
    UserUid = user.uid;
    DB_users = firebase.database().ref(`users`);
    DB_users_uid = firebase.database().ref(`users/${UserUid}`);
    DB_chats = firebase.database().ref(`chats`);

    DB_users_uid.once('value').then(snapshot => {
      UserName = snapshot.val().name;
      UserImg = snapshot.val().img;

      initNavbar();
      initFriends();
      initChats();
      initAddFriend();
      initTalk();
      initTalkListener();
    });
  });
}