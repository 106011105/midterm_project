function init() {
  let btnLogin = document.getElementById('btnLogin');
  let btnGoogle = document.getElementById('btnGoogle');
  let btnSignUp = document.getElementById('btnSignUp');

  btnLogin.addEventListener('click', () => {
    let email = document.getElementById('inputEmail').value;
    let password = document.getElementById('inputPassword').value;
    firebase.auth().signInWithEmailAndPassword(email, password).then(() => {
      document.location.href = './index.html';
    }).catch(error => alert(error));
  });

  btnGoogle.addEventListener('click', () => {
    let provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(result => {

      let loginUser = result.user;
      firebase.database().ref('users').once('value').then(snapshot => {
        let newUser = true;
        snapshot.forEach(el => {
          let uid = el.key;
          if (uid === loginUser.uid) {
            newUser = false;
          }
        });
        if (newUser) {
          firebase.database().ref('users/' + loginUser.uid).set({
            email: loginUser.email,
            name: loginUser.displayName,
            img: 'user1.png',
            friends: {
              group: [{name: 'PlayGround', img: 'group3.png'}],
              friend: []
            }
          });
        }
        setTimeout(() => {
          document.location.href = './index.html'
        }, 1000);
      })
    }).catch(error => alert(error));
  });

  btnSignUp.addEventListener('click', () => {
    document.location.href = './signup.html'
  });
}

window.onload = () => {
  init();
}